## MakerDifference Overview

Thanks to funding from Arts Council England Liverpool Libraries are running a new programme of Maker activities in libraries this year. Building on the success of Make Fest and Liverpool Code Club the Maker Difference project will bring more opportunities for young people to engage in making across the city.

There will be four new mini maker spaces, with 3d printing, vinyl cutting and electronics kit available in Central, Toxteth, Speke and Norris Green libraries. This new kit will be supported by workshops and new code clubs to help introduce new makers to the tech.This funding will also support Make Fest on 24th June and also a smaller Young Makefest in March 2018. There will also be commissioned projects by Domestic Science, Re-dock and WeHearTech

Support from DoES Liverpool, Chris Huffee and LittleSandbox has been instrumental in this bid and we will continue to support Maker Difference throughout the year.
