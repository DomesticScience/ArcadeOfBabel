# Arcade of Babel
### Press Release and Main Project Information 

[The Arcade of Babel](https://domesticscience.org.uk/gamejam.html) is an arcade of text based computer games that explore how we read in public and use libraries.

It is a ***[MakerDifference](https://twitter.com/makerdiff)*** commission, supported by **Arts Council England**.

A series of FREE participatory **Game-jams** for the public will prototype games made playable in game cabinets, echoing the arcades of the 1980s.

These games will inform the co-design of an RFID tag driven text adventure game using [AVATAP](https://github.com/cefn/avatap), a game system Domestic Science developed with [ShrimpingIt](http://start.shrimping.it).

AVATAP was developed for [Milecastles](http://milecastles.uk) as a new form of text based gaming that rewards physical exploration and reading. Part story, part digital trail, part treasure hunt, players use their RFID adventure tag to discover the game and the many books and collections of Liverpool Libraries.

The game will be playable to the public in Liverpool Central Library in March 2018 as part of the ***[MakerDifference](https://twitter.com/makerdiff)*** Programme.

## Game-jam!

<img width="300" src="http://domesticscience.org.uk/images/oksparks2.png">

Our **game-jam** is a **free** event for young people and families to make games with artists for the arcade and get them played in a special arcade cabinets on display in Liverpool and help co-design the RFID text adventure using the free webapp ***[Twine](http://twinery.org)***.

<img width="300" src="https://pbs.twimg.com/profile_images/909829970888929281/ATDR0UPR_400x400.jpg">

The first [game-jam](#) takes place at the **MakerDifference Day Launch** on **Saturday 18th November 2017** times **10:30am - 3.30pm** at ***[Toxteth Library](https://liverpool.gov.uk/libraries/find-a-library/toxteth-library/)***.

## Hackday!

Our HackDay is a free event for participants on our game-jams who want to work more in depth and is much more advanced than the game-jam work. We will be developing games with people from the Northern creative tech community inspired by gamejam games and co-building the RFID text game for the public in Liverpool Central Library.

The first HackDay is on ***Saturday 2nd December 2017*** times **10:30am - 4:30pm**

See [Other All the other Dates here](Dates.md) and on the [Website](http://domesticscience.org.uk/gamejam.html)

Register for the FREE HackDay at links below and drop in to any of our game-jams!

[HackDay 1](https://www.eventbrite.co.uk/e/arcade-of-babel-game-hackday-tickets-39400043591)
[HackDay 2](https://www.eventbrite.co.uk/e/arcade-of-babel-game-hackday-tickets-42462836487)


## Background

It comes out of DomesticScience's interest in [Interactive Non Fiction](http://slides.com/cheapjack/inf) and @cheapjack's [ForkingLibraries](https://github.com/cheapjack/ForkingLibraries) project research into using text games in Libraries and refers to ["The Library of Babel"](https://en.wikipedia.org/wiki/The_Library_of_Babel) story by librarian and writer [Jorge Louis Borges](https://en.wikipedia.org/wiki/Jorge_Luis_Borges)

He made a game about it [The Library of Forking Paths](http://domesticscience.org.uk/library.html) inspired by [Library of Babel As A Service](https://libraryofbabel.info/) by [Jonothan Basile](https://twitter.com/JonotrainEB). 

