# Arcade of Babel

Source files and management of Game Jams with young people and DomesticScience to make Twine games in arcade game cabinet and a new Avatap game for Liverpool Central Library

Arcade of Babel is a project for [MakerDifference](https://twitter.com/MakerDiff) to augment reading in libraries. 

# Background

It comes out of DomesticScience's interest in [Interactive Non Fiction](http://slides.com/cheapjack/inf) and @cheapjack's [ForkingLibraries](https://github.com/cheapjack/ForkingLibraries) project research into using text games in Libraries and refers to ["The Library of Babel"](https://en.wikipedia.org/wiki/The_Library_of_Babel) story by librarian and writer [Jorge Louis Borges](https://en.wikipedia.org/wiki/Jorge_Luis_Borges)

He made a game about it [The Library of Forking Paths](http://domesticscience.org.uk/library.html) inspired by [Library of Babel As A Service](https://libraryofbabel.info/) by [Jonothan Basile](https://twitter.com/JonotrainEB). 

## Project
### Game Jam

Jam to introduce participants to Twine and make some library themed games.

The games are then shown in a DomesticScience game cabinet.

### Development

Based on these games we will make an RFID driven text game using the [avatap](https://github.com/cefn/avatap) game system Domestic Science used for [Milecastles](http:milecastles.uk) to encourage exploring and browsing in libraries.