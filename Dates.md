## Dates for Arcade of Babel

Activity|Date|Location
-----|-----|-----
GameJam 1|18/11/17|Toxteth
HackDay 1|2/12/17|Toxteth
GameJam 2|27/1/18|Toxteth
HackDay 2|17/2/18|Liverpool Central Library
GameJam 3|10/2/18|Liverpool Central Library
GameJam 4|23/3/18|Liverpool Central Library
