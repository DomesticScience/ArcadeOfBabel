## Arcade of Babel

Source files and management of Game Jams with young people and DomesticScience to make Twine games in arcade game cabinets and a new Avatap game for Liverpool Central Library

 * [DomesticScience Webpage for Arcade of Babel](http://domesticscience.org.uk)
 * [Main Press Release doc Explaining Project](https://gitlab.com/DomesticScience/ArcadeOfBabel/blob/master/PressRelease.md)
 * [Project Overview](https://gitlab.com/DomesticScience/ArcadeOfBabel/blob/master/ProjectOverview.md) for our own notes of the structure of the project
 * [Game Jam](https://gitlab.com/DomesticScience/ArcadeOfBabel/blob/master/GameJam.md) our own notes of the structure of the game jam.
