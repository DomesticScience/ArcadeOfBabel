# Arcade of Babel: Library Game Jam!

Come along to the **Arcade of Babel** Game Jam at MakerDifference Day on Saturday 28th November at [Toxteth Library](https://liverpool.gov.uk/libraries/find-a-library/toxteth-library/) Windsor Street, L8 1XF 
10.30 - 16.00

## Whats a game-jam?

It's a day when people get together to quickly prototype and make computer games!

Make text based games with [Domestic Science](http://domesticscience.org.uk) about reading and exploring libraries and get them played in a special arcade cabinet on display at Toxteth and central library.

Games made will inform building a special text adventure game hidden in the collections of Liverpool Central Library 

## Background

Thanks to funding from Arts Council England Liverpool Libraries are running a new programme of digital making activities in libraries this year. Building on the success of Liverpool MakeFest and Liverpool Code Club the Maker Difference project will bring more opportunities for young people to engage in making across the city.

Domestic Science and The Arcade of Babel project is one of the artists commissioned to make work for Maker Difference 

